/****************************************
 *   Wzorzec Projektowy Builder          *
 *   (budowniczy)                        *
 *   www.algorytm.org                    *
 *   Opracowal Dworak Kamil              *
 *   POŻYCZYŁ SEBASTIAN WRÓBEL           *
 *****************************************/


import java.util.*;

public class Main {
    public static void main(String[] args) {
        Director szef = new Director();
        Builder builder = new ZestawXT001();
        Builder builder2 = new ZestawABC996();

        System.out.println("\nZESTAW1");
        szef.setBuilder(builder);
        szef.skladaj();
        ZestawKomputerowy zestaw1 = szef.getZestaw();

        szef.setBuilder(builder2);
        szef.skladaj();
        ZestawKomputerowy zestaw2 = szef.getZestaw();

        zestaw1.show();
        System.out.println("\n\nZESTAW2");
        zestaw2.show();
    }
}