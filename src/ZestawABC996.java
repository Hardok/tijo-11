public class ZestawABC996 extends Builder {

    public void buildMonitor() {
        zestawKomputerowy.setMonitor("LG");
    }

    public void buildProcesor() {
        zestawKomputerowy.setProcesor("INTEL");
    }

    public void buildGrafika() {
    }

    public void buildRam() {
        zestawKomputerowy.setRam("DDR");
    }

    public void buildHdd() {
        zestawKomputerowy.setHdd("Samsung");
    }
}